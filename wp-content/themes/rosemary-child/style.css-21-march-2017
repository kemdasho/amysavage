/*
 Theme Name:   Rosemary Child
 Template:     rosemary
*/

@import url('https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i');


h1, h2, h3, h4, h5, h6 
{
	font-family: 'Montserrat', arial;
	color: #444;
}
.post-entry h1,h1
{
	font-size: 24px;
}
.post-entry h2,h2
{
	font-size: 22px;
}
.post-entry h3,h3
{
	font-size:20px; 
}
h4
{
	font-size: 18px;
}
.post-entry h5,h5
{
	font-size: 16px;
}
.post-entry h6,h6
{
	font-size:14px;
}
body
{
	font-family: 'Montserrat Light', arial;
	color:#444;
}


/**** HEADER CSS START ****/

/*#logo img
{
	width: 522px;
}*/

#header
{
	border-bottom: none;
}

#top-bar
{
	box-shadow: none;
}

#content
{
	margin: 0px;
}

ul#menu-main-menu
{
	padding:13px 0 0 0; 
}

#nav-wrapper .menu li:first-child 
{
	border: none;
	margin-right: 0;
	padding: 0 15px 0 0;
}

#nav-wrapper .menu li:last-child 
{
	margin-right: 22px;
	padding: 0 15px;
}

#nav-wrapper .menu li
{
	margin-right: 0;
	padding: 0 15px;
}

#nav-wrapper .menu li a
{
	font-size:12px;
	line-height: 24px;
	font-weight: 600;
}

#nav-wrapper .menu li.current_page_item > a
{
	font-weight:400;
}

#nav-wrapper .sub-menu li
{
	border:none;
}

#nav-wrapper .sub-menu li:first-child
{
	border: none;
	padding-left:14px;
}

#nav-wrapper .sub-menu li:last-child
{
	margin-right: 0px;
}

#top-search input::-webkit-input-placeholder
{
   color: #000;
}

#top-search input:-moz-placeholder { /* Firefox 18- */
   color: #000;  
}

#top-search input::-moz-placeholder {  /* Firefox 19+ */
   color: #000;  
}

#top-search input:-ms-input-placeholder {  
   color: #000;  
}

#top-search i
{
	cursor:pointer;
}

#top-search 
{
	position: static; 
	border:none;
	right: 0;
	top: 0;
}

#top-social 
{
	position: absolute;
	right: 0;
	top: 0;
}

#top-social a i 
{
	font-size: 14px;
}

.banner-container
{
	/*background: url("images/home-banner.jpg") left top no-repeat;*/
	padding:11% 0; 
	margin: 0 0 40px 0;
	background-repeat:no-repeat;
	background-size:cover;
}

/**** HEADER CSS END ****/


/**** HOME MAIN CONTENT CSS START ****/

#main-container
{
	width: 100%;
	float:left;
}

.post-entry h1, 
.post-entry h2, 
.post-entry h3, 
.post-entry h4, 
.post-entry h5, 
.post-entry h6
{
	color:#444;
}

.post-header h2 a, .post-header h1
{
	color:#444;
	font-size:24px;
}

.grid-item .post-header h2 a 
{
	font-size: 24px;
	font-weight: 400;
	color:#444;
}

.grid-item .post-header .cat a
{
	color: #777777;
	border-color: #777777;
	font-size: 14px;
	letter-spacing: 0;
}

.post-img
{
	margin: 0 0 10px 0;
}

/*.sp-grid .post-img img
{
	height: 300px;
}
*/

.post-meta 
{
	margin: 0 0 20px 0;
	border-bottom: 1px solid #cccccc;
}

.meta-comments a 
{
	color: #333;
}

.sp-grid .meta-share 
{
	color: #333;
	width: 50%;
}

.share-text 
{
	margin-right: 61px;
	color: #333;
}

.meta-share a 
{
	color: #444;
}

.more-button 
{
	font-size: 14px;
	letter-spacing: 0;
	font-weight: 400;
	padding: 5px 10px;
}

.sp-grid .post-entry p
{
	margin-bottom: 18px;
	font-size: 14px;
	color: #444;
	text-align: center;
}

ul.sp-grid.recp-cat p 
{
	text-align: center;
}

.pagination 
{
	margin-top: 0px;
	overflow: hidden;
	margin-bottom: 40px;
}

.post-title 
{
	text-align: center;
	font-size: 30px;
	color: #444;
	font-weight: 400;
	margin: 0 0 30px 0;
	text-decoration: underline;
}

.recipe-title
{
	text-align: center;
	font-size: 30px;
	color: #444;
	font-weight: 400;
	margin: 0 0 30px 0;
	text-decoration: underline;
}

/**** HOME MAIN CONTENT CSS END ****/


/****  RECIPE PAGE CSS START 	****/

.post-entry ul.recp-cat 
{
   
	 padding-left: 0;

}

.recp-cat .post-header .cat a
{

    color: #777777;
    border-color: #777777;

}

.post-entry a:hover 
{

    text-decoration: none;

}

.print-cat-container 
{
	width: 100%;
	float: left;
	margin: 0 0 20px 0;
}

.print-container
{
	float: left;
	padding: 8px;
	border: 1px solid #000;
	border-radius: 5px;
}

.print-link
{
	background: url("images/print-icon.png") left center no-repeat;
	color: #000;
	font-size: 16px;
	font-weight: 600;
	padding: 0px 0 0 30px;
	float: left;
}

.cat-dropdown-container 
{
	float: right;
}

.cat-dropdown-container select
{
	border: 1px solid #ddd;
	color: #888;
	padding: 9px 7px;
	width: 100%;
}

.post-header h1 
{
	text-decoration: underline;
}

.page-id-82 .sp-grid li,
.page-id-81 .sp-grid li
{
	margin-bottom:30px;
}

/****  RECIPE PAGE CSS END 	****/

/****  POST RELATED CSS START 	****/
.post-related .item-related h3 
{
    text-align: center;
}
.post-related .item-related span.date 
{
   
    display: block;
    text-align: center;
}
.post-related .item-related .post-img 
{
    margin-top: 10px;
}
/****  POST RELATED CSS END 	****/

/**** NUTRITION PAGE CSS START ****/

.question-container 
{
	width: 100%;
	float: left;
}

.questions-box 
{
	width: 100%;
	margin: 0 0 20px 0;
	float: left;
}

.questions-box .post-header h2
{
	font-size: 20px;
	float: left;
	color: #444;
	font-weight: 700;
	width: 100%;
	text-align: left;
	margin: 10px 0 25px;
	text-transform: capitalize;
}

.qustion-thumbnail
{
	width: 100%;
	float: left;
}

.answer-container
{
	width: 100%;
	float: left;
}

.questions-box strong
{
	float: left;
	padding: 2px 4px 0 0;
	text-decoration: underline;
}

.question-container .cat a
{
	color:#777;
	border-bottom: 1px solid #777;
	
    font-size: 10px;
    letter-spacing: 2px;
    padding-bottom: 2px;
    display: inline-block;
text-transform: capitalize;
} 

/**** NUTRITION PAGE CSS END ****/



/**** SIDEBAR CSS START ****/


#solopine_about_widget-2 
{
	border: 1px solid #eee;
	padding: 0 0 20px;
}

#solopine_about_widget-2 p 
{
	color: #444;
	line-height: 25px;
	font-size: 15px;
	text-align: center;
}

#solopine_about_widget-2 a 
{
	border: 1px solid #ccc;
	padding: 5px 10px;
	display: inline-block;
	margin-top: 10px;
	color: #333;
	font-size: 14px;
	text-transform: uppercase;
}

.widget-title 
{
	border: 1px solid;
	border-color: #444;
	font-size: 24px;
	color: #fff;
	text-transform: capitalize;
	font-family: 'Montserrat', arial;
	margin-bottom: 25px;
	background: #444;
	font-weight: 400;
}
div#mc4wp_form_widget-2 
{
    border: 1px solid #eee;
    padding: 0 0 20px;
}
.mc4wp-form-fields p 
{
    color: #444;
    line-height: 25px;
    font-size: 13px;
    padding: 0px 0 20px;
    text-align: center;
    width: 100%;
}
.mc4wp-form-fields label
{
	display: none;
}
.mc4wp-form-fields input[type="email"]
{
	margin: 0px auto;
	width: 85%;
	background-color: #f4f4f4;
}
.mc4wp-form-fields input[type="submit"] 
{
    font-size: 18px;
    color: #fff;
    background: #444;
    margin: 0px auto;
    border: none;
    width: 60%;
    font-family: 'Montserrat Light', arial;
}
.mc4wp-alert.mc4wp-notice p 
{
    text-align: center;
}
div#newsletterwidget-2 
{
	border: 1px solid #eee;
	padding: 0 0 20px; 
}

#newsletterwidget-2 p 
{
	color: #444;
	line-height: 25px;
	font-size: 13px;
	padding: 0px 0 20px;
	text-align: center;
	float: left;
	width: 100%;
}

#newsletterwidget-2 label
{
	display: none;
}

#newsletterwidget-2 .tnp-email
{
	margin: 0px auto;
	width: 85%;
	background-color: #f4f4f4;
}

#newsletterwidget-2 .tnp-widget input.tnp-submit 
{
	font-size: 18px;
	color: #fff;
	background: #444;
	margin: 0px auto;
	border: none;
	width: 60%;
	font-family: 'Montserrat Light', arial;
}

#solopine_social_widget-2
{
	border: 1px solid #eee;
}

.social-widget
{
	padding: 10px 0px 30px 0px;
}

.social-widget a
{
	/*border: 1px solid #ccc;*/
	padding: 16px 2px 8px 0px;
	margin: 0 3px;
}

.social-widget a i 
{
	margin: 0 8px;
	font-size: 24px;
	color:#444;
}


/**** SIDEBAR CSS END ****/



/**** FOOTER CSS START ****/


/**** INSTAGRAM CSS START ****/

#instagram-footer 
{
	background: #F8F8F8;
	padding: 58px 51px;
}

.instagram-title 
{
	font-size: 26px;
	color: #444;
	margin: 0 0 35px 0;
	padding: 0 0 10px;
	background: url(images/border.jpg) no-repeat bottom center
}

/**** INSTAGRAM CSS START ****/



/**** FOOTER FOUR COLUMN CSS START ****/

.footer-four-column
{
	background: #333333;
	float: left;
	width: 100%;
}

.footer-four-column-inner
{
	width: 100%;
	padding:34px 0 40px 0; 
	float: left;
}

.footer-widget-title
{
	float: left;
	width: 100%;
	font-size: 20px;
	color: #fff;
	text-transform: uppercase;
	margin:  0 0 30px;
	font-weight: normal;
	background: url(images/border.jpg) no-repeat left bottom;
	padding-bottom: 5px;
}

.column-one 
{
	float: left;
	width: 24%;
	margin-right: 40px;
}

.column-one p
{
	float: left;
	width: 100%;
	font-size: 15px;
	color: #8a8a8a;
	line-height: 22px;
	text-align: justify;
	margin: 10px 0;
}

.column-one img
{
	width: 100%;
}

.column-two 
{
	width: 18%;
	float: left;
	margin-right: 40px;
}

.column-two ul li
{
	display: inline-block;
	width: 100%;
	border-bottom: 1px solid #444444;
	padding: 7px 0;
}

.column-two ul li:last-child
{
	border-bottom:none;
}

.column-two ul li a 
{
	color: #999999;
	font-size: 13px;
	text-transform: uppercase;
}

.column-three 
{
	width: 22%;
	float: left;
	margin-right: 40px;
}

.column-three ul li 
{
	border-bottom: 1px solid #444444;
	padding-bottom: 20px;
	margin-bottom: 20px;
}

.column-three ul li:last-child
{
	border-bottom:none;
}

.column-three ul li a 
{
	color: #999999;
	text-transform: uppercase;
	font-size: 13px;
}

.column-three ul li .post-date 
{
	color: #FFF;
	display: block;
	padding: 5px 0 0 0;
}

.column-four
{
	float: left;
	width: 24%;
}

.column-four ul li 
{
	float: left;
	width: 100%;
	border-bottom: 1px solid #444444;
	padding-bottom: 20px;
	margin-bottom: 20px;
}

.column-four ul li:last-child 
{
	border-bottom: none;
}

.side-image 
{
	width: 20%;
	float: left;
}

.column-four .side-item-thumb.wp-post-image 
{
	width: 60px;
	height: 60px;
	float: left;
}

.column-four .side-item-text
{
	float: left;
	width: 72%;
}

.column-four .side-item-text a
{
	color: #999999;
	padding:5px 0; 
	text-transform: uppercase;
	font-size: 13px;
	float: left;
}

.column-four .side-item-meta
{
	color: #FFF;
	font-size: 13px;
	float: left;
	clear:both;
}

.single p span
{
	display: none;
}

/**** FOOTER FOUR COLUMN CSS END ****/



/**** COPYRIGHT CSS START ****/

#footer
{
	background: #000;
	float: left;
	width: 100%;
}

#footer .copyright.left
{
	float: left;
	color: #FFF;
	font-size: 15px;
	text-align: center;
	font-style: normal;
	width: 100%;
}

/**** COPYRIGHT CSS END ****/


/**** FOOTER CSS END ****/


.h5ab-print-button.h5ab-print-button-left
{
	margin: 20px 0 !important;
	border: 1px solid #ddd;
	border-radius: 3px;
	padding: 6px 13px;
}

.h5ab-print-button.h5ab-print-button-left span 
{
	float: right;
	padding: 0 0 0 6px;
}


/** Hide home page second title  **/
.home .recipe-container h2.recipe-title 
{
    display: none;
}

/**** MEDIA QUERY CSS START ****/


@media (max-width: 1024px)
{

	.share-text
	{
		margin-right: 33px;
	}

	.column-one
	{
		margin-right:30px;
	}

	.column-two
	{
		margin-right:30px;
	}

	.column-three
	{
		margin-right:30px;
	}

	.column-four
	{
		margin-right:20px;
	}

	.column-four .side-item-text 
	{
		width: 68%;
		margin-left: 25px;
	}

	.banner-container 
	{
		background-size: 100%;
	}

}


@media (max-width: 960px)
{

	#top-search 
	{
		position: absolute;
		right: 45%;
	}

	.share-text
	{
		margin-right: 55px;
	}

	.column-one
	{
		width: 45%;
	}

	.column-two
	{
		width: 45%;
	}

	.column-three
	{
		width: 45%;
		margin-top:20px;
		clear:left;
	}

	.column-four
	{
		width: 45%;
		margin-top:20px;
	}

}



@media (max-width: 800px)
{

	#top-bar 
	{
		display: none;
	}

    #header #logo 
	{
		padding: 18px 0 12px;
	}

	#header #logo img 
	{
		max-width: 65%;
	}


	#responsive-menu-container *
	{
		width: 100%;
	}

	#header
	{
		padding-top:0px; 
	}

	#responsive-menu #responsive-menu-item-154,
	#responsive-menu #responsive-menu-item-155,
	#responsive-menu #responsive-menu-item-156,
	#responsive-menu #responsive-menu-item-157,
	#responsive-menu #responsive-menu-item-198,
	#responsive-menu #responsive-menu-item-215,
	#responsive-menu #responsive-menu-item-319
	{
		display: inline-block;
		width: auto;
		padding: 10px 14px;
		text-align: center;
	}
    

    #responsive-menu-container li.responsive-menu-item 
    {
		width: 100%;
		list-style: none;
	}

    div#responsive-menu-container i 
    {
		font-size: 16px;
	}

	#responsive-menu-container #responsive-menu #responsive-menu-item-154 a:hover,
	#responsive-menu-container #responsive-menu #responsive-menu-item-155 a:hover,
	#responsive-menu-container #responsive-menu #responsive-menu-item-156 a:hover,
	#responsive-menu-container #responsive-menu #responsive-menu-item-157 a:hover,
	#responsive-menu-container #responsive-menu #responsive-menu-item-198 a:hover,
	#responsive-menu-container #responsive-menu #responsive-menu-item-215 a:hover
	{
		background-color:transparent;
	}

	.mobile-logo 
	{
		width: 120px !important;
		margin: -5px 0 0 0px !important;
	}

    .recipe-title
    {
    	font-size:24px;
    }

    .post-title
    {
    	font-size:24px;
    }

    #responsive-menu-container #responsive-menu-additional-content
    {
    	padding:6px 5% 0 !important; 
    }

    #responsive-menu-container #responsive-menu li.responsive-menu-item a .responsive-menu-subarrow:hover
    {
    	background: transparent;
    	color: #444;
        border-color: transparent;
    }

    #responsive-menu-container #responsive-menu li.responsive-menu-item a .responsive-menu-subarrow
    {
		color: #444 !important;
		border-left: 1px solid transparent !important;
		background-color: transparent !important;
	}

    #responsive-menu-additional-content
    {
    	text-align:center;
    }

    button#responsive-menu-button:focus 
    {
		outline: none;
	}

}



@media (max-width: 768px)
{


	#top-search 
	{
		position: absolute;
		right: 45%;
	}

	.sp-grid .meta-share 
	{
		width: 60%;
	}

	.share-text 
	{
		margin-right: 0px;
	}

	.column-one
	{
		width: 100%;
		margin: 0px;
	}

	.column-two
	{
		width: 100%;
		margin: 20px 0;
	}

	.column-three
	{
		width: 100%;
		margin: 0px;
	}

	.column-four
	{
		width: 100%;
		margin: 0px;
	}

	.post-header h2 a, .post-header h1
	{
		color:#444;
		font-size:18px;
	}


}



@media (max-width: 480px)
{

	.share-text 
	{
		margin-right: 44px;
	}


}
