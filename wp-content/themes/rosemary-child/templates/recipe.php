<?php
/*
Template Name: Recipes Post Page
*/

/**************** Recipe posts *********************/

get_header();
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$args = array(
	'post_type' => 'recipe',
	'posts_per_page'=> 8,
	'order' => 'DESC',	
	'paged' => $paged,
); 

//print_r($args);
$recipe = new WP_Query($args);

?>

<div id="main-container">
<div class="container">
		
<div id="content">

<div id="main">

<div class="post-header">
						
<h1><?php echo get_the_title(); ?></h1>
						
</div>

<ul class="sp-grid recp-cat">				

<?php

if ( $recipe->have_posts()): while( $recipe->have_posts()):  $recipe->the_post(); ?>

 <li>
	<div class="post-header">
		<!--<span class="cat">
		<?php
		/*$terms = get_the_terms( $recipe->ID , 'recipes_category' );
		foreach ( $terms as $term ) 
		{
            $term_link = get_term_link( $term );
			echo '<a href="'.esc_url( $term_link ).'">'.$term->name.'</a>';

		}*/
		?>
		</span>-->
	    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
	</div>

	<?php if(has_post_format('gallery')) : ?>
	
		<?php $images = get_post_meta( $post->ID, '_format_gallery_images', true ); ?>
		
		<?php if($images) : ?>
		<div class="post-img">
		<ul class="bxslider">
		<?php foreach($images as $image) : ?>
			
			<?php $the_image = wp_get_attachment_image_src( $image, 'full-thumb' ); ?> 
			<?php $the_caption = get_post_field('post_excerpt', $image); ?>
			<li><img src="<?php echo esc_url($the_image[0]); ?>" <?php if($the_caption) : ?>title="<?php echo $the_caption; ?>"<?php endif; ?> /></li>
			
		<?php endforeach; ?>
		</ul>
		</div>
		<?php endif; ?>
	
	<?php elseif(has_post_format('video')) : ?>
	
		<div class="post-img">
			<?php $sp_video = get_post_meta( $post->ID, '_format_video_embed', true ); ?>
			<?php if(wp_oembed_get( $sp_video )) : ?>
				<?php echo wp_oembed_get($sp_video); ?>
			<?php else : ?>
				<?php echo $sp_video; ?>
			<?php endif; ?>
		</div>
	
	<?php elseif(has_post_format('audio')) : ?>
	
		<div class="post-img audio">
			<?php $sp_audio = get_post_meta( $post->ID, '_format_audio_embed', true ); ?>
			<?php if(wp_oembed_get( $sp_audio )) : ?>
				<?php echo wp_oembed_get($sp_audio); ?>
			<?php else : ?>
				<?php echo $sp_audio; ?>
			<?php endif; ?>
		</div>
	
	<?php else : ?>
		
		<?php if(has_post_thumbnail()) : ?>
		<?php if(!get_theme_mod('sp_post_thumb')) : ?>
		<div class="post-img">
			<a href="<?php echo get_permalink() ?>"><?php the_post_thumbnail('singlepost-thumb'); ?></a>
		</div>
		<?php endif; ?>
		<?php endif; ?>
		
	<?php endif; ?>

    <?php if(get_theme_mod('sp_post_comment_link') && get_theme_mod('sp_post_share')) : else : ?>	
	<div class="post-meta">
		
		<?php if(!get_theme_mod('sp_post_comment_link')) : ?>		
		<div class="meta-comments">
			<?php comments_popup_link( '0 Comments', '1 Comment', '% Comments', '', ''); ?>
		</div>
		<?php endif; ?>
		
		<?php if(!get_theme_mod('sp_post_share')) : ?>
		<div class="meta-share">
		    <?php
			$wc_pin_image = '';
			if(has_post_format('gallery'))
			{
				
				$images = get_post_meta( $post->ID, '_format_gallery_images', true );
				foreach($images as $image)
				{
					$the_image = wp_get_attachment_image_src( $image, 'singlepost-thumb' );
					$wc_pin_image = esc_url($the_image[0]);
				}
			}
			else
			{
				$wc_pin_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID));	
			}

			?>
			<span class="share-text"><?php _e( 'Share', 'solopine' ); ?></span>
			<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><i class="fa fa-facebook"></i></a>
			<a target="_blank" href="https://twitter.com/intent/tweet?text=Check%20out%20this%20article:%20<?php print solopine_social_title( get_the_title() ); ?>&url=<?php echo urlencode(the_permalink()); ?>"><i class="fa fa-twitter"></i></a>
			<?php 
			$pin_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
			print_r($pin_image); 
			?>
			<a data-pin-do="none" target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php echo urlencode(the_permalink()); ?>&media=<?php echo esc_url($wc_pin_image); ?>&description=<?php print solopine_social_title( get_the_title() ); ?>"><i class="fa fa-pinterest"></i></a>
			<!--<a target="_blank" href="https://plus.google.com/share?url=<?php the_permalink(); ?>"><i class="fa fa-google-plus"></i></a>-->
		</div>
		<?php endif; ?>
		
	</div>
	<?php endif; ?>
						
		<?php if(get_theme_mod('sp_post_summary') == 'excerpt') : ?>
				
				<p style="min-height: 70px !important;"><?php echo sp_string_limit_words(get_the_excerpt(), 20); ?>&hellip;</p>
				<p><a href="<?php echo get_permalink() ?>" class="more-link"><span class="more-button"><?php _e( 'READ MORE', 'solopine' ); ?></span></a>
				</p>
		<?php endif; ?>


</li>

<?php

endwhile;

endif;

?>

</ul>

<?php if ($recipe->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
  <nav class="pagination">
    <div class="older">
      <?php echo get_next_posts_link( 'Older Posts', $recipe->max_num_pages ); // display older posts link ?>
    </div>
    <div class="newer">
      <?php echo get_previous_posts_link( 'Newer Posts' ); // display newer posts link ?>
    </div>
  </nav>
<?php } ?>

</div>
<?php 
get_sidebar();
?>
</div>
</div>
</div>
<?php
get_footer();


