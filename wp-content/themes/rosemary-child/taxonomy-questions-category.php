<?php get_header(); ?>
	
	<div class="archive-box">
	
		<span><?php _e( 'Browsing Question Category', 'solopine' ); ?></span>
		<h1><?php printf( __( '%s', 'solopine' ), single_cat_title( '', false ) ); ?></h1>
		
	</div>
	
	<div class="container">
		
		<div id="content">
		
			<div id="main">
			
				
			<div class="question-container">				

<?php


if ( have_posts()): while( have_posts()):  the_post(); ?>

    <div class="questions-box">
	<div class="post-header">
		<span class="cat">
		<?php
		$terms = get_the_terms( '', 'questions-category' );
		foreach ( $terms as $term ) 
		{
            $term_link = get_term_link( $term );
			echo '<a href="'.esc_url( $term_link ).'">'.$term->name.'</a>';

		}
		?>
		</span>
	    <h2>Q. <?php the_title(); ?></h2>
	</div>
		
	<div class="qustion-thumbnail">
		<?php if(has_post_thumbnail()) : ?>
		<?php if(!get_theme_mod('sp_post_thumb')) : ?>
		<div class="post-img">
			<?php the_post_thumbnail('full-thumb'); ?>
		</div>
		<?php endif; ?>
	<?php endif; ?>
	</div>

    <div class="answer-container">					
		<?php echo "<strong>Ans: </strong>"; the_content();?>
    </div>

    </div>

<?php

endwhile;

endif;

?>

</div>

	
</div>

<?php if(get_theme_mod('sp_sidebar_archive')) : else : ?><?php get_sidebar(); ?><?php endif; ?>
<?php get_footer(); ?>