	
		<!-- END CONTENT -->
		</div>
		
	<!-- END CONTAINER -->
	</div>
	
	<div id="instagram-footer">
		
		<?php	/* Widgetised Area */	if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('Instagram Footer') ) ?>
		
	</div>
	
	<div class="footer-four-column">
	  <div class="container">
	      <div class="footer-four-column-inner">
	      
	      <div class="column-one">
	      		<?php if ( is_active_sidebar( 'first-footer-widget-area' ) ) : 
	              
	                dynamic_sidebar( 'first-footer-widget-area' );
	              
	            endif; ?>
	      </div>

	      <div class="column-two">
	      		<?php if ( is_active_sidebar( 'second-footer-widget-area' ) ) : 
	              
	                dynamic_sidebar( 'second-footer-widget-area' );
	              
	            endif; ?>
	      </div>

	      <div class="column-three">
	      		<?php if ( is_active_sidebar( 'third-footer-widget-area' ) ) : 
	              
	                dynamic_sidebar( 'third-footer-widget-area' );
	              
	            endif; ?>
	      </div>

	      <div class="column-four">
	      		<?php if ( is_active_sidebar( 'fourth-footer-widget-area' ) ) : 
	              
	                dynamic_sidebar( 'fourth-footer-widget-area' );
	              
	            endif; ?>
	      </div>

	      </div>
	  </div>
	</div>

	<div id="footer">
		
		<div class="container">
			
			<div class="copyright left">
				<?php if ( is_active_sidebar( 'copyright_widget' ) ) : 
		              
		                dynamic_sidebar( 'copyright_widget' );
	              
	            endif; ?>
			</div>
			<?php //echo wp_kses_post(get_theme_mod('sp_footer_copyright_left')); ?>
			<p class="copyright right"><?php echo wp_kses_post(get_theme_mod('sp_footer_copyright_right')); ?></p>
			
		</div>
		
	</div>
	
	<?php wp_footer(); ?>
	
<script type="text/javascript">
$("img").attr( "data-pin-nopin", "true" );
$(".single article img").removeAttr( "data-pin-nopin");
$(".answer-container img").removeAttr( "data-pin-nopin");
</script>

<script>
    $("#searchform").hide();
	$(document).ready(function()
	{
		$(".search-desktop").click(function()
		{
			$("#searchform").toggle();
		});
	});


	jQuery( document ).ready(function() 
	{
        jQuery(".wpp-date").text(function () {
			 return jQuery(this).text().replace("posted on", ""); 
	    });
	});
</script>

</body>


</html>

<style type="text/css">
	.mc4wp-form-fields input[type="submit"] {-webkit-appearance: none; appearance: none; -moz-appearance: none; border-radius: 0; -moz-border-radius: 0; -webkit-border-radius: 0;}
</style>