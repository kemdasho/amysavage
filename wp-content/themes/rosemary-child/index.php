<?php get_header(); ?>
	<div id="main-container">
	<div class="container">
		
		<?php if(get_theme_mod( 'sp_featured_slider' ) == true) : ?>
			<?php get_template_part('inc/featured/featured'); ?>
		<?php endif; ?>
		
		<div id="content">
		
			<div id="main" <?php if(get_theme_mod('sp_sidebar_homepage') == true) : ?>class="fullwidth"<?php endif; ?>>
			
			<!--BLOG CONTAINER START-->

			<div class="recipe-container">
						    <h2 class="recipe-title">Latest Blogs</h2>
				<?php if(get_theme_mod('sp_home_layout') == 'grid' || get_theme_mod('sp_home_layout') == 'full_grid') : ?><ul class="sp-grid"><?php endif; ?>
				
				<?php $args = array(
					'post_type' => 'post',
					'order' => 'ASC',	
					'post_per_page' => 8,
					'ping_status' => 'open'
				);

				//print_r($args);
				$recipe = new WP_Query($args);


				if ( $recipe->have_posts()): while( $recipe->have_posts()):  $recipe->the_post(); ?>
					
					<?php if(get_theme_mod('sp_home_layout') == 'grid') : ?>
					
						<?php get_template_part('custom', 'grid'); ?>
					
					<?php elseif(get_theme_mod('sp_home_layout') == 'list') : ?>
					
						<?php get_template_part('custom', 'list'); ?>
						
					<?php elseif(get_theme_mod('sp_home_layout') == 'full_list') : ?>
					
						<?php if( $recipe->current_post == 0 && !is_paged() ) : ?>
							<?php get_template_part('customcontent'); ?>
						
							<?php get_template_part('custom', 'list'); ?>
						<?php endif; ?>
					
					<?php elseif(get_theme_mod('sp_home_layout') == 'full_grid') : ?>
					
						<?php if( $recipe->current_post == 0 && !is_paged() ) : ?>
							<?php get_template_part('customcontent'); ?>
						<?php else : ?>
							<?php get_template_part('custom', 'grid'); ?>
						<?php endif; ?>
					
					<?php else : ?>
						
						<?php get_template_part('customcontent'); ?>
						
					<?php endif; ?>	
				<?php
                endwhile;
                wp_reset_postdata();
				endif;

				?>		
				
				
				<?php if(get_theme_mod('sp_home_layout') == 'grid' || get_theme_mod('sp_home_layout') == 'full_grid') : ?></ul><?php endif; ?>
				
					<?php //solopine_pagination(); ?>
				
				

			</div>	<!--BLOG CONTAINER END-->
 
 
			</div>
            
<?php if(get_theme_mod('sp_sidebar_homepage')) : else : ?><?php get_sidebar(); ?>
</div>
<?php endif; ?>
<?php get_footer(); ?>
