<?php get_header(); ?>
	
	<div id="main-container">
	<div class="container">
		
		<div id="content">
		
			<div id="main">
			
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
					<?php get_template_part('content', 'page'); ?>
						
				<?php endwhile; ?>
				
				<?php endif; ?>
				
			</div>

			<?php get_sidebar(); ?>
			</div>


</div>
</div>
<?php get_footer(); ?>