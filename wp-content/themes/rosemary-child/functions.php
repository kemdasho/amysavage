<?php
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}

function four_column_widget_init() {
 
    // First footer widget area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'First Footer Widget Area', 'solopine' ),
        'id' => 'first-footer-widget-area',
        'description' => __( 'The first footer widget area', 'solopine' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="footer-widget-title">',
        'after_title' => '</h3>',
    ) );
 
    // Second Footer Widget Area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Second Footer Widget Area', 'solopine' ),
        'id' => 'second-footer-widget-area',
        'description' => __( 'The second footer widget area', 'solopine' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="footer-widget-title">',
        'after_title' => '</h3>',
    ) );
 
    // Third Footer Widget Area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Third Footer Widget Area', 'solopine' ),
        'id' => 'third-footer-widget-area',
        'description' => __( 'The third footer widget area', 'solopine' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="footer-widget-title">',
        'after_title' => '</h3>',
    ) );
 
    // Fourth Footer Widget Area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Fourth Footer Widget Area', 'solopine' ),
        'id' => 'fourth-footer-widget-area',
        'description' => __( 'The fourth footer widget area', 'solopine' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="footer-widget-title">',
        'after_title' => '</h3>',
    ) );
         
}
 
// Register sidebars by running tutsplus_widgets_init() on the widgets_init hook.
add_action( 'widgets_init', 'four_column_widget_init' );


function copyright_widgets_init() {

    register_sidebar( array(
        'name' => 'Copyright Widget',
        'id' => 'copyright_widget',
        'before_widget' => '<div id="copy-widget">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="rounded">',
        'after_title' => '</h2>',
    ) );
}
add_action( 'widgets_init', 'copyright_widgets_init' );


add_filter('widget_text','execute_php',100);
function execute_php($html){
     if(strpos($html,"<"."?php")!==false){
          ob_start();
          eval("?".">".$html);
          $html=ob_get_contents();
          ob_end_clean();
     }
     return $html;
}


/*---------------------------- modified on 28 jan 2017 --------------------------------*/

function print_button_shortcode( $atts ){
return '<a class="print-link" href="javascript:window.print()">Print This Page</a>';
}
add_shortcode( 'print_button', 'print_button_shortcode' );

function post_title_shortcode($atts){
    $value = shortcode_atts( array(
        'offset' => 0,
    ), $atts );
    $args = array( 
        'post_type' => 'recipe',
        'numberposts' => 1,
        'post_status' => 'publish',
        'offset' => $value['offset']
         );
    $myposts = wp_get_recent_posts( $args);
    if(empty($myposts)){
        return '<div class="et_pb_text_inner">No post Found</div>';
    }else{
        foreach($myposts as $mypost){
            return '<div class="et_pb_text_inner"><h3 style="padding-bottom:0;">'.$mypost['post_title'].'</h3></div>';
        }
    }
}


// START CODE FOR GALLARY IMAGE FOR FRONTEND DISPLAY
function post_picture_shortcode( $atts = ''){
    $value = shortcode_atts( array(
        'offset' => 0,
    ), $atts );
    $args = array( 
        'post_type' => 'recipe',
        'numberposts' => 1,
        'post_status' => 'publish',
        'offset' => $value['offset']
         );
    $myposts = wp_get_recent_posts( $args);
    if(empty($myposts)){
        return '<div class="et_pb_text_inner">No posts Found</div>';
    }else{
        //$media = get_attached_media('image', $myposts[0]['ID']);
        $images = get_post_meta( $myposts[0]['ID'], '_format_gallery_images', true );
        $the_image = wp_get_attachment_image_src( $images[0], 'full-thumb' );
        $counter = 0;
        return '<img src="'.$the_image[0].'">';    
        /*foreach($media as $med){
            if($counter == (sizeof($media)-1)){
                return '<img src="'.$med->guid.'">';
                // return '<div class="et_pb_text_inner">'.$med->guid.'?resize=1080</div>';
            }
            $counter++;
        }*/
    }
}

// END CODE FOR GALLARY IMAGE FOR FRONTEND DISPLAY

function post_read_more_shortcode($atts = ''){
    $value = shortcode_atts( array(
        'offset' => 0,
    ), $atts );
    $args = array( 
        'post_type' => 'recipe',
        'numberposts' => 1,
        'post_status' => 'publish',
        'offset' => $value['offset']
         );
    $myposts = wp_get_recent_posts( $args);
    if(empty($myposts)){
        return '<div class="et_pb_text_inner">No post Found</div>';
    }else{
        foreach($myposts as $mypost){
            return ' <a class="et_pb_button et_pb_button_0 et_pb_bg_layout_light" href="'.$mypost['guid'].'">READ MORE</a>';
        }
    }
}
add_shortcode( 'post_title1', 'post_title_shortcode' );
add_shortcode( 'post_picture1', 'post_picture_shortcode' );
add_shortcode( 'post_readmore1', 'post_read_more_shortcode' );

// add_theme_support( 'post-thumbnails' );
add_filter( 'jpeg_quality', create_function( '', 'return 100;' ) );
//add_image_size( 'singlepost-thumb', 365, 300, true );

// function add_custom_sizes() {
//     add_image_size( 'singlepost-thumb', 365, 300, true );
// }
// add_action('after_setup_theme','add_custom_sizes');



