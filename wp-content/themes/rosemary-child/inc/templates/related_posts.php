<?php 

$orig_post = $post;
global $post;

$categories = get_the_category($post->ID);

if ($categories) {

	$category_ids = array();

	foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
	
	$args = array(
		'category__in'     => $category_ids,
		'post__not_in'     => array($post->ID),
		'posts_per_page'   => 3, // Number of related posts that will be shown.
		'ignore_sticky_posts' => 1,
		'orderby' => 'rand'
	);

	$my_query = new wp_query( $args );
	if( $my_query->have_posts() ) { ?>
		<div class="post-related"><div class="post-box"><h4 class="post-box-title"><?php _e('You Might Also Like', 'solopine'); ?></h4></div>
		<?php while( $my_query->have_posts() ) {
			$my_query->the_post();?>
				<div class="item-related">
					
					<!--<?php if ( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) : ?>
					<a href="<?php echo get_permalink() ?>"><?php the_post_thumbnail('misc-thumb'); ?></a>
					<?php endif; ?>-->

					<h3><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
					<span class="date"><?php the_time( get_option('date_format') ); ?></span> 

					<!---edit by vikram -->
					
					<?php if(has_post_format('gallery')) : ?>
	
		                     <?php $images = get_post_meta( $post->ID, '_format_gallery_images', true ); ?>
		
		                        <?php if($images) : ?>
		                        <div class="post-img">
		                            <ul class="bxslider">
		                              <?php foreach($images as $image) : ?>
			
		                            	<?php $the_image = wp_get_attachment_image_src( $image, 'singlepost-thumb' ); ?> 
			                            <?php $the_caption = get_post_field('post_excerpt', $image); ?>
			                            <li><img src="<?php echo esc_url($the_image[0]); ?>" <?php if($the_caption) : ?>title="<?php echo $the_caption; ?>"<?php endif; ?> /></li>
			
		                              <?php endforeach; ?>
		                              <?php endif; ?>
		                            </ul>
		                        </div>
		            <?php endif; ?>
		            <!---end edit by vikram -->
					
				</div>
		<?php
		}
		echo '</div>';
	}
}
$post = $orig_post;
wp_reset_query();

?>