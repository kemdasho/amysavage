
/* global jetpack_recipes_vars */
!function(e){e(window).load(function(){e(".jetpack-recipe-print a").click(function(i){i.preventDefault(),e(this).closest(".jetpack-recipe").printThis({pageTitle:jetpack_recipes_vars.pageTitle,loadCSS:jetpack_recipes_vars.loadCSS})})})}(jQuery);